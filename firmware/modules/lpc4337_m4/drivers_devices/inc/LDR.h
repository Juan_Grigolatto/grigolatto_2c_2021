/* Copyright 2021,
 * Juan M. Grigolatto
 * juangrigolatto1603@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef LDR_H
#define LDR_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup LDR LDR
 ** @{ */

/** @brief Bare Metal header for LDR on EDU-CIAA NXP
 **
 ** This driver measures the luminosity of the environment in lux.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	JMG			Juan M. Grigolatto
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211113 v0.1 initials initial version by Juan
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>

/*==================[macros]=================================================*/
#define CH0 1  /*Board connector: DAC pin*/
#define CH1 2  /*Board connector: CH1 pin*/
#define CH2 4  /*Board connector: CH2 pin*/
#define CH3 8  /*Board connector: CH3 pin*/

#define SINGLE_MODE	0
#define BURST_MODE	1
/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/
/** @fn void LDRInit(uint8_t CH, uint8_t mode)
 * @brief LDR initialization
 * @param[in] CH input channel
 * @param[in] mode measure mode (single-burst)
 * @return null
 */
void LDRInit(uint8_t CH, uint8_t mode);
/** @fn uint16_t LDRRead()
 * @brief Measure the luminosity.
 * @return luminosity value
 */
uint16_t LDRRead();
/** @fn uint16_t ToLux(uint16_t value_to_lux)
 * @brief Convert a digital word to lux
 * @param[in] value_to_lux digital word to convert
 * @return value converted in lux
 */
uint16_t ToLux(uint16_t value_to_lux);

/*==================[end of file]============================================*/
#endif /* #ifndef LED_H */

