	/* Copyright 2021,
 * Maria Casablanca
 * mariacasabla@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef HC_SR4_H
#define HC_SR4_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Ultrasonido Ultrasonido
 ** @{ */

/** @brief Bare Metal header for Ultrasound on EDU-CIAA NXP
 **
 ** This is a driver for Ultrasound
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	MC         Maria Casablanca
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210901 v0.1 initials initial version Maria Casablanca
 */

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/

/** @fn bool HcSr04Init(gpio_t echo, gpio_t trigger)
 * @brief Inicializacion funcion del Ultrasonido de la  EDU-CIAA
 * @param[in] echo  Pulso de salida (Output)
 * @param[in] trigger Pulso de entrada (Input)
 * @return TRUE si no hay error
 */

bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @fn uint16_t HcSr04ReadDistanceCentimeters(void)
 * @brief Mide la distancia a un objeto
 * @param[in] No Parametro
 * @return Distancia en centimetros
 */
uint16_t HcSr04ReadDistanceCentimeters(void);

/** @fn uint16_t HcSr04ReadDistanceInches(void)
 * @brief Mide la distancia a un objeto
 * @param[in] No Parametro
 * @return Distancia en pulgadas
 */
uint16_t HcSr04ReadDistanceInches(void);

/** @fn bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
 * @brief Desinicializacion funcion del Ultrasonido de la  EDU-CIAA
 * @param[in] echo  Pulso de salida (Output)
 * @param[in] trigger Pulso de entrada (Input)
 * @return TRUE si no hay error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef HC_SR4_H */

