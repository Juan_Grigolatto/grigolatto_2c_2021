/* Copyright 2017, Sebastian Pablo Bedin <sebabedin@gmail.com>
 * Copyright 2018, Eric Pernia.
 * All rights reserved.
 *
 * This file is part sAPI library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup DHT11 sapi_dht11
 ** @{ */

/** @brief Bare Metal header for DHT11 on EDU-CIAA NXP
 **
 ** This driver measures temperature and relative humidity of the environment.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	JMG			Juan M. Grigolatto
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211113 v0.1 initials initial version by Juan
 */

/* Date: 2017-11-13 */

#ifndef _DHT11_H_
#define _DHT11_H_

#include "gpio.h"
#include "bool.h"
/** @fn void dht11Init( gpio_t gpio )
 * @brief Initialization of DHT11 Sensor
 * @param[in] gpio port of input/output.
 * @return NULL
 */
void dht11Init( gpio_t gpio );
/** @fn _Bool dht11Read( float *phum, float *ptemp )
 * @brief measures temperature and humidity
 * @param[in] phum save humidity value
 * * @param[in] ptemp save temperature value
 * @return TRUE if no error
 */
_Bool dht11Read( float *phum, float *ptemp );

#endif /* _DHT11_H_ */
