/* Copyright 2021,
 * Grigolatto, Juan Marcos
 * juangrigolatto1603@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef DISPLAYITS_E0803_H
#define DISPLAYITS_E0803_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Display
 ** @{ */

/** @brief Driver for show numbers in a display ITS-E0803 in the EDU-CIAA board.
 **
 ** this is a driver for a display ITS-E0803.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	JMG			Juan M. Grigolatto
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210912 v0.1 initials initial version Juan Grigolatto.
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/


/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/



/*==================[external functions declaration]=========================*/

/** @fn bool ITSE0803Init(gpio_t * pins)
 * @brief Inicialización función del display de la  EDU-CIAA
 * @param[in] *pins vector que contiene los pines correspondientes a los puertos y las lineas de control.
 * @return TRUE si no hay error
 */
bool ITSE0803Init(gpio_t * pins);
/** @fn bool ITSE0803DisplayValue(uint16_t valor)
 * @brief Muestra en el display un valor.
 * @param[in] valor contiene el número que se desea mostrar en la pantalla.
 * @return TRUE si no hay error
 */
bool ITSE0803DisplayValue(uint16_t valor);
/** @fn uint16_t ITSE0803ReadValue(void)
 * @brief Devuelve el valor que se muestra en la pantalla.
 * @return valor de la pantalla.
 */
uint16_t ITSE0803ReadValue(void);
/** @fn bool ITSE0803Deinit(gpio_t * pins)
 * @brief desinicialización función del display de la  EDU-CIAA
 * @param[in] *pins vector que contiene los pines correspondientes a los puertos y las lineas de control.
 * @return TRUE si no hay error
 */
bool ITSE0803Deinit(gpio_t * pins);

/*==================[end of file]============================================*/
#endif  #ifndef DISPLAYITS_E0803_H

