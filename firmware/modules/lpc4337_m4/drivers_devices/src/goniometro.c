	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Convierte valores de tensión de un divisor resistivo a grados.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *JG			Juan Grigolatto
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version Juan Grigolatto
 */

/*==================[inclusions]=============================================*/
#include "goniometro.h"
#include "gpio.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/
gpio_t global_pin;
uint16_t channel;
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
/** @fn void GoniometroInit(uint8_t CH, uint8_t mode)
 * @brief Inicializa el goniometro y configura el conversor analogico digital.
 * @param[in] CH guarda el pin para la entrada analogica
 * @return None
 */
void GoniometroInit(uint8_t CH, uint8_t mode){
	analog_input_config analog_config;
	analog_config.input=CH;
	analog_config.mode=AINPUTS_SINGLE_READ;
	analog_config.pAnalogInput=0;
	AnalogInputInit(&analog_config);
	channel=CH;
}
/** @fn uint16_t GoniometroRead()
  * @brief Lee el valor analogico de tensión de un divisor resistivo y lo convierte a digital
  * @return retorna el valor en grados (debería, no está la conversión, devuelve un valor digital)
  */
uint16_t GoniometroRead(){
	uint16_t read_value;
	//uint16_t converted_value;
	AnalogInputReadPolling(channel, &read_value);
	//converted_value=ConvertToDegree(read_value);
	return read_value;
	//return converted_value;
}
/** @fn uint16_t ConvertToDegree(uint16_t value_to_convert)
 * @brief Convierte valores digitales del goniometro a grados.
 * @param[in] CH guarda el valor digital que debe convertir a grados.
 * @return valor convertido a grados.
 */
uint16_t ConvertToDegree(uint16_t value_to_convert){
	
}

/*==================[end of file]============================================*/
