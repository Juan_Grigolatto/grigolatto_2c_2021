/* Copyright 2021,
 * Grigolatto, Juan Marcos
 * juangrigolatto1603@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Driver for show numbers in a display ITS-E0803 in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 * JMG			Juan M. Grigolatto
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20210912 v0.1 initials initial version Juan Grigolatto
 */

/*==================[inclusions]=============================================*/
#include "DisplayITS_E0803.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
uint8_t global_value; /*Guarda el valor del número que se desea mostrar.*/
uint8_t global_pins[7]; /*Guarda los pines del display.*/
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
void BcdToPin (uint8_t number);
void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number );
/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
void BcdToPin (uint8_t number){
	/*Enciende los pins correspondientes para mostrar el número en el display (requiere que el valor sea en bcd)*/
	uint8_t mascara=1;
	uint8_t auxiliar=0;

	auxiliar=number&mascara;

	if(auxiliar){
		GPIOOn(global_pins[0]);
	}

	else{
		GPIOOff(global_pins[0]);
	}

	mascara=mascara << 1;

	auxiliar=number&mascara;

	if(auxiliar){
		GPIOOn(global_pins[1]);
	}

	else{
		GPIOOff(global_pins[1]);
	}

	mascara=mascara << 1;

	auxiliar=number&mascara;

	if(auxiliar){
		GPIOOn(global_pins[2]);
	}

	else{
		GPIOOff(global_pins[2]);
	}

	mascara=mascara << 1;

	auxiliar=number&mascara;

	if(auxiliar){
		GPIOOn(global_pins[3]);
	}

	else{
		GPIOOff(global_pins[3]);
	}




}

void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	/*transforma el número de binario a bcd.*/

	uint8_t i=0, aux;
		uint32_t aux_num;
		aux_num=data;
		uint8_t aux_digits;
		aux_digits=digits;
		while(aux_digits>0){
		aux=(aux_num%10);
		aux_num=(aux_num/10);
		bcd_number[aux_digits-1]=aux;
		aux_digits--;

		}

	/*uint8_t i=0, aux;
	uint32_t aux_num;
	aux_num=data;
	for(i=0;i<digits; i++){
		aux=(aux_num%10);
		aux_num=(aux_num/10);
		bcd_number[i]=aux;

	}*/

}


/*==================[external functions definition]==========================*/

bool ITSE0803Init(gpio_t * pins){
	/*Configuración del gpio*/
	uint8_t i;
	for(i=0; i<7; i++){
		GPIOInit(pins[i],GPIO_OUTPUT);
		global_pins[i]=pins[i];
	}

	for(i=0; i<7; i++){
		GPIOOff(pins[i]);
	}

	return true;
}
bool ITSE0803DisplayValue(uint16_t valor){
	/*Muestra el valor en el display*/

	uint8_t bcd_number[3];
		BinaryToBcd (valor, 3, bcd_number);
	uint8_t i=0;
	global_value=valor;
		for (i=0; i<3; i++){
			GPIOOff(global_pins[i]);
			BcdToPin(bcd_number[i]);
			GPIOOn(global_pins[i+4]);
			GPIOOff(global_pins[i+4]);
		}

	return true;
}

uint16_t ITSE0803ReadValue(void){
	/*Devuelve el valor que se muestra en el display.*/
	return global_value;
}

bool ITSE0803Deinit(gpio_t * pins){
	GPIODeinit();
}



/*==================[end of file]============================================*/
