	/* Copyright 2021,
 * Juan M. Grigolatto
 * juangrigolatto1603@hotmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for LDR in the EDU-CIAA board.
 **	This driver measures the luminosity of the environment in lux.
 **/

/*
 * Initials     Name
 * ---------------------------
 *	JMG			Juan M. Grigolatto
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211113 v0.1 initials initial version Juan Grigolatto
 */

/*==================[inclusions]=============================================*/
#include "LDR.h"
#include "gpio.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
#define DARK_RESISTANCE 1000   		/*Resistance value of LDR in darkness*/
#define LIGHT_RESISTANCE 15    		/*Resistance value of LDR (10 lux)*/
#define CALIBRATION_RESISTANCE 10 	/*Resistance of calibration value of LDR*/
#define CONVERTION_FACTOR 10 		/*Convertion factor*/



uint8_t channel;
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
void LDRInit(uint8_t CH, uint8_t mode){

	analog_input_config analog_config;
	analog_config.input=CH;
	analog_config.mode=AINPUTS_SINGLE_READ;
	analog_config.pAnalogInput=0;
	AnalogInputInit(&analog_config);
	channel=CH;

}

uint16_t LDRRead(){
	uint16_t lux_value;
	uint16_t read_value;
	AnalogInputReadPolling(channel, &read_value);
	lux_value=ToLux(read_value);
	return lux_value;
}

uint16_t ToLux(uint16_t value_to_lux){
	uint16_t convert_value;
	convert_value=(value_to_lux*DARK_RESISTANCE*CONVERTION_FACTOR)/(LIGHT_RESISTANCE*CALIBRATION_RESISTANCE*(1024-value_to_lux));
	return convert_value;
}

/*==================[end of file]============================================*/
