/* Copyright 2016,
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version Leando Medus
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 */

/*==================[inclusions]=============================================*/
#include "DHT22.h"
#include "gpio.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/
#define START_WAIT_TIME 10
#define MCU_START_SIGNAL_TIME 18
#define DHT_WAIT_TIME 18
#define DHT_RESPONSE_TIME 80
#define DHT_PREPARATION_DATA_TIME 80
#define TIME_VALUE_DATA 29
#define DELAY_TIME 10
#define CONVERTION_VALUE_TO_RH 10





gpio_t gpio_global_pin;
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
void DHTInit(gpio_t gpio_pin){
	//GPIOInit(gpio_pin,GPIO_OUTPUT);
	gpio_global_pin=gpio_pin;
}
uint16_t DHTRead(){

	uint16_t humidity_value=0;
	uint16_t before_comma_value=0;
	uint16_t after_comma_value=0;
	uint16_t high_time=0;
	uint16_t wait_time=0;
	uint16_t high_time_2=0;
	uint16_t wait_time_2=0;
	uint16_t low_check=0;
	uint16_t high_check=0;
	uint16_t data_bit[40];
	uint8_t i;

	StartSignal();
	//DelayUs(DELAY_TIME);
	//DelayUs(DHT_RESPONSE_TIME);
	//DelayUs(DHT_PREPARATION_DATA_TIME);
	while(!GPIORead(gpio_global_pin)){
		wait_time_2++;
		low_check=1;
	}
	while(GPIORead(gpio_global_pin)){
		high_time_2++;
		high_check=1;
	}
	DelayUs(DELAY_TIME);
	//if(high_check==1){
	for(i=0;i<40;i++){
		high_time=0;
		wait_time=0;
		while(!GPIORead(gpio_global_pin)){
			DelayUs(1);
			wait_time++;
		}
		while(GPIORead(gpio_global_pin)){
			DelayUs(1);
			high_time ++;
		}

		//data_time[i]=high_time;
		if(high_time<TIME_VALUE_DATA){
			data_bit[i] = 0;
		}
		else{
			data_bit[i] = 1;
		}
		if(i<8){
			before_comma_value = data_bit[i] << (8-i);
		}
		if(8<=i  && i<16){
			after_comma_value = data_bit[i] << (16-i);
		}
	}
	//}
	//DelayUs(10);
	//GPIOInit(gpio_global_pin,GPIO_OUTPUT);
	//GPIOOn(gpio_global_pin);


	return before_comma_value;
}

void StartSignal(){
	GPIOInit(gpio_global_pin,GPIO_OUTPUT);
	GPIOOn(gpio_global_pin);
	DelayUs(START_WAIT_TIME);
	GPIOOff(gpio_global_pin);
	DelayMs(MCU_START_SIGNAL_TIME);
	GPIOOn(gpio_global_pin);
	DelayUs(DHT_WAIT_TIME);
	GPIOInit(gpio_global_pin,GPIO_INPUT);
}

/*==================[end of file]============================================*/
