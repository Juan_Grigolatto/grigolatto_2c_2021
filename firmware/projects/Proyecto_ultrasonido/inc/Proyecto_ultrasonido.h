/*! @mainpage Proyecto_ultrasonido
 *
 * \section genDesc General Description
 *
 *	The application measures distance from a object and show the measure.
 *	turn on the led in a logical order depending the measure.
 *	This application can hold the measure for a indeterminate time or measure
 *	indefinitely.
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * |	GND	 		| 	GND			|
 * |	ECHO	 	| 	T_FIL2		|
 * |	TRIGGER	 	| 	T_FIL3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 29/09/2021 | Document creation		                         |
 *
 * @author Juan Grigolatto
 *
 */

#ifndef _PROYECTO_ULTRASONIDO_H
#define _PROYECTO_ULTRASONIDO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

