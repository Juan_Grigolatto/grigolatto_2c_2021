/*! @mainpage Proyecto_ultrasonido
 *
 * \section genDesc General Description
 *
 * The application measures distance from a object and show the measure.
 * turn on the leds in a logical order depending the measure.
 * This application can hold the measure for a indeterminate time or measure
 * indefinitely.
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * |	GND	 		| 	GND			|
 * |	ECHO	 	| 	T_FIL2		|
 * |	TRIGGER	 	| 	T_FIL3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 29/09/2021 | Document creation		                         |
 *
 * @author Juan Grigolatto
 *
 */

/*==================[inclusions]=============================================*/
#include "Proyecto_ultrasonido.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "systemclock.h"
#include "hc_sr4.h"
#include "led.h"
#include "delay.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/
uint16_t distance=0;
uint8_t  measure=0;
uint8_t estado_actual=0;
uint8_t estado_ant=0;
uint8_t hold=1;
uint8_t pins[]={GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	ITSE0803Init(pins);
	uint8_t teclas;
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	while(1){


		teclas  = SwitchesRead();
		estado_actual=teclas;
		if(estado_actual!=estado_ant){
			DelayMs(5);
			estado_ant=estado_actual;
		}
		switch (teclas){
		case SWITCH_1:
			LedOn(LED_RGB_G);
			if(measure==0){
				measure=1;
			}
			else{
				measure=0;
			}
			break;
		case SWITCH_2:
			LedOn(LED_RGB_R);
			if(hold==0){
				hold=1;

			}
			else{
				hold=0;
			}
			break;
		}
		if(measure==1){

			distance=HcSr04ReadDistanceCentimeters();
			if(hold==1){


				if(distance<10){
					LedOn(LED_RGB_B);

					LedOff(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
				}
				if(distance<20 && distance>10){
					LedOn(LED_1);
					LedOn(LED_RGB_B);

					LedOff(LED_2);
					LedOff(LED_3);
				}
				if(distance<30 && distance>20){
					LedOn(LED_2);
					LedOn(LED_1);
					LedOn(LED_RGB_B);

					LedOff(LED_3);
				}
				if(distance>30){
					LedOn(LED_3);
					LedOn(LED_2);
					LedOn(LED_1);
					LedOn(LED_RGB_B);
				}

				ITSE0803DisplayValue(distance);
			}

		}
		else{

			LedOff(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
		}


	}

	return 0;
}




/*==================[end of file]============================================*/

