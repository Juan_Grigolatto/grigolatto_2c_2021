/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Prueba_PWM_pro_final.h"       /* <= own header */
#include "pwm_sct.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/

pwm_out_t pwm_configuration[]={CTOUT0,CTOUT1};
pwm_out_t pwm_light={CTOUT0};
pwm_out_t pwm_humidity={CTOUT1};
/*==================[external functions definition]==========================*/
void SisInit(){


	PWMSetDutyCycle(pwm_light, 50);
	PWMOn();
	PWMSetDutyCycle(pwm_humidity, 50);
	PWMOn();

}
int main(void){
	SisInit();
    
	return 0;
}

/*==================[end of file]============================================*/

