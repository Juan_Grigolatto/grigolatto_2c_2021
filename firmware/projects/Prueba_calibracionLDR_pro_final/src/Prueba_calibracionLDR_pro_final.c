/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Prueba_calibracionLDR_pro_final.h"       /* <= own header */
#include "uart.h"
#include "delay.h"
#include "LDR.h"
#include "systemclock.h"
#include "pwm_sct.h"
/*==================[macros and definitions]=================================*/
serial_config configuration;
uint32_t value;
uint16_t cycle=0;
pwm_out_t pwm_configuration[]={CTOUT0};
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
void SisInit(void)
{

	configuration.port=SERIAL_PORT_PC;
	configuration.baud_rate=115200;
	configuration.pSerial=0;
	SystemClockInit();
	UartInit(&configuration);
	LDRInit(CH1, SINGLE_MODE);
	PWMInit(pwm_configuration,1,1000);
	PWMOn();
}

int main(void){
	SisInit();
	while(cycle<=100){
		cycle++;
		value=LDRRead();
		PWMSetDutyCycle(CTOUT0,cycle);
		UartSendString(SERIAL_PORT_PC, "Luminosidad: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(value, 10));
		UartSendString(SERIAL_PORT_PC, " lux\r\n");

		UartSendString(SERIAL_PORT_PC, "Ciclo: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(cycle, 10));
		UartSendString(SERIAL_PORT_PC, " %\r\n");

		DelayMs(500);
	}
	while(1){


	}

	return 0;
}

/*==================[end of file]============================================*/

