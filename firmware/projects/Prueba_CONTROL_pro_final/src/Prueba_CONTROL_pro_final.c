/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Prueba_CONTROL_pro_final.h"       /* <= own header */
#include "uart.h"
#include "LDR.h"
#include "pwm_sct.h"
/*==================[macros and definitions]=================================*/
uint16_t light_value;

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/
pwm_out_t pwm_light={CTOUT0};

/*==================[external functions definition]==========================*/

void MeasureLight(){
	light_value=LdrRead();

}

void SisInit(){
	UartInit(&uart_configuration);
	LdrInit(CH1,SINGLE_MODE);
	PWMInit(pwm_configuration, 1, 1000);
}
int main(void){

	while(1){

		UartSendString(SERIAL_PORT_PC, UartItoa(light_value, 10));
		UartSendString(SERIAL_PORT_PC, "% \r\n");

		//UartSendString(SERIAL_PORT_PC, UartItoa(humidity_value, 10));
		//UartSendString(SERIAL_PORT_PC, "% \r\n");


		if(light_value>set_light){
			LedOn(LED_1);
			while(light_value>set_light){
				pwm_cycle_value=pwm_cycle_value_light-1;
				PWMSetDutyCycle(pwm_light, pwm_cycle_value_light);
				MeasureLight();
				LedOff(LED_1);
			}
		}
		if(light_value<set_light){
			LedOn(LED_2);
			while(light_value<set_light){
				pwm_cycle_value=pwm_cycle_value_light+1;
				PWMSetDutyCycle(pwm_light, pwm_cycle_value_light);
				MeasureLight();
			LedOff(LED_2);
			}
		}

		/*if(humidity_value<set_humidity){
			LedOn(LED_1);
			while(humidity_value<set_humidity){
				pwm_cycle_value=pwm_cycle_value_hum+1;
				PWMSetDutyCycle(pwm_humidity, pwm_cycle_value_hum);
				MeasureHumidity();
			}
			LedOff(LED_1);
		}
		if(humidity_value>set_humidity){
			LedOn(LED_2);
			while(humidity_value>set_humidity){
				pwm_cycle_value=pwm_cycle_value_hum-1;
				PWMSetDutyCycle(pwm_humidity, pwm_cycle_value_hum);
				MeasureHumidity();
			}
			LedOff(LED_2);
		}*/

	}


	return 0;
}

/*==================[end of file]============================================*/

