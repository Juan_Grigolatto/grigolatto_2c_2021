/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Prueba_MENU_pro_final.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "delay.h"
#include "uart.h"
#include "sapi_rtc.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
rtc_t hour_config;
rtc_t operation_time_config;
typedef enum  {hour_p, minutes_p, day_p, month_p, year_p} time_t;

time_t current_time=hour_p;
time_t operation_time=hour_p;


bool set_light_flag=FALSE;
bool set_humidity_flag=FALSE;
bool set_time_flag=FALSE;
bool set_operation_time_flag=FALSE;
bool camera_on=FALSE;

bool light_value_in=FALSE;
bool humidity_value_in=FALSE;
bool operation_time_in=FALSE;

uint8_t set_light = 0;
uint8_t set_humidity = 0;

/*==================[internal functions declaration]=========================*/
void Menu(void){

	uint8_t data;
	UartReadByte(SERIAL_PORT_PC, &data);

	if(set_humidity_flag){
		set_humidity = data;
		if((set_humidity<=100) && (set_humidity>=0)){
			UartSendString(SERIAL_PORT_PC, "Humedad relativa:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(data, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			data = 0;
			set_humidity_flag = FALSE;
			humidity_value_in = TRUE;
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato invalido...ingrese un valor entre 0 y 100\r\n");
			data = 0;
		}
	}
	if(set_light_flag){
		set_light = data;
		if((set_light<=100) && (set_light>=0)){
			UartSendString(SERIAL_PORT_PC, "Luminosidad:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(data, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			data = 0;
			set_light_flag = FALSE;
			light_value_in = TRUE;

		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato invalido...ingrese un valor entre 0 y 100\r\n");
			data = 0;
		}
	}
	if(set_time_flag){
		switch(current_time){
		case hour_p:
			hour_config.hour = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese la minutos actuales:  \r\n");
			current_time = minutes_p;
			break;
		case minutes_p:
			hour_config.min = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese el dia actuales:  \r\n");
			current_time = day_p;
			break;
		case day_p:
			hour_config.mday = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese el mes actual:  \r\n");
			current_time = month_p;
			break;
		case month_p:
			hour_config.month = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese el anio actual:  \r\n");
			current_time = year_p;
			break;
		case year_p:
			hour_config.year = data;
			UartSendString(SERIAL_PORT_PC, "Son las:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.min, 10));
			UartSendString(SERIAL_PORT_PC, "  del:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.mday, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.month, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.year, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			current_time = hour_p;
			data = 0;
			set_time_flag = FALSE;
			LedOff(LED_RGB_B);
			LedOn(LED_RGB_G);
			break;
		}
	}

	if(set_operation_time_flag){
		switch(operation_time){
		case hour_p:

			//operation_time_config.hour = data;
			//UartSendString(SERIAL_PORT_PC, "Ingrese los minutos:  \r\n");
			//operation_time = minutes_p;
			break;
		case minutes_p:
			operation_time_config.min = data;
			operation_time_config.mday= hour_config.mday;
			operation_time_config.month= hour_config.month;
			operation_time_config.year= hour_config.year;
			operation_time = hour_p;
			data = 0;
			set_operation_time_flag= FALSE;
			operation_time_in = TRUE;
			UartSendString(SERIAL_PORT_PC, "Tiempo de operacion:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(operation_time_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(operation_time_config.min, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			break;
		}
	}
	switch(data){
	case 'H':
		UartSendString(SERIAL_PORT_PC, "Ingrese la humedad relativa:\r\n");
		set_humidity_flag= TRUE;
		break;
	case 'L':
		UartSendString(SERIAL_PORT_PC, "Ingrese el valor de luminosidad:\r\n");
		set_light_flag= TRUE;
		break;
	case 'I':
		if(light_value_in==TRUE && humidity_value_in==TRUE && operation_time_in==TRUE){
			UartSendString(SERIAL_PORT_PC, "Camara encendida\r\n");
			UartSendString(SERIAL_PORT_PC, "Ingrese la hora actual:\r\n");
			set_time_flag= TRUE;
			LedOn(LED_RGB_B);
			camera_on=true;
		}
		else{
			if(!light_value_in){
				UartSendString(SERIAL_PORT_PC, "Error: ingrese la luminosidad\r\n");
			}
			if(!humidity_value_in){
				UartSendString(SERIAL_PORT_PC, "Error: ingrese la humedad relativa\r\n");
			}
			if(!operation_time_in){
				UartSendString(SERIAL_PORT_PC, "Error: ingrese el tiempo de operacion\r\n");
			}
			UartSendString(SERIAL_PORT_PC, "Luego, vuelva a intentarlo\r\n");

		}
		break;
	case 'F':
		LedOff(LED_RGB_G);
		if(camera_on){
			camera_on=false;
			UartSendString(SERIAL_PORT_PC, "Camara apagada\r\n");
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Primero debe encender la camara\r\n");
		}
		break;
	case 'D':
		UartSendString(SERIAL_PORT_PC, "Ingrese las horas:\r\n");
		set_operation_time_flag= TRUE;
		break;
	case 'h':
		UartSendString(SERIAL_PORT_PC, "Humedad relativa:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(set_humidity, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		break;
	case 'l':
		UartSendString(SERIAL_PORT_PC, "Luminosidad:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(set_light, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		break;
	case 'd':
		UartSendString(SERIAL_PORT_PC, "Tiempo de operacion:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(operation_time_config.hour, 10));
		UartSendString(SERIAL_PORT_PC, ":");
		UartSendString(SERIAL_PORT_PC, UartItoa(operation_time_config.min, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		break;
	case 'M':
		UartSendString(SERIAL_PORT_PC, "/---------------MENU------------/ \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'I' para encender la camara y configurar la hora \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'F' para apagar la camara\r\n");

		UartSendString(SERIAL_PORT_PC, "Ingrese 'H' para configurar la humedad relativa: \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'L' para configurar la luminosidad: \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'D' para configurar la duracion: \r\n");

		UartSendString(SERIAL_PORT_PC, "Ingrese 'h' para recuperar la humedad relativa: \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'l' para recuperar la luminosidad: \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'd' para recuperar la duracion: \r\n");
		UartSendString(SERIAL_PORT_PC, "/-------------------------------/ \r\n");
		break;
	}
}



/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();
	LedsInit();

	serial_config UART_config;
	UART_config.baud_rate = 115200;
	UART_config.port = SERIAL_PORT_PC;
	UART_config.pSerial = Menu;
	UartInit(&UART_config);
	UartSendString(SERIAL_PORT_PC, "Ingrese 'M' para acceder al menu de operacion\r\n");
	while(1){

	}

	return 0;
}

/*==================[end of file]============================================*/

