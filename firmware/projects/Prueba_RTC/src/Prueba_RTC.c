/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Prueba_RTC.h"       /* <= own header */
#include "sapi_rtc.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/

rtc_t hour_config;
rtc_t operation_time_config;
/*==================[external functions definition]==========================*/

int main(void){


	hour_config.hour = 5;
	hour_config.sec = 0;
	hour_config.min = 24;
	hour_config.mday = 9;
	hour_config.month = 11;
	hour_config.year = 21;
	operation_time_config.hour = 5;
	operation_time_config.min = 26;
	rtcConfig(&hour_config);
	while(1){
		rtcRead(&hour_config);
	}

	return 0;
}

/*==================[end of file]============================================*/

