/*! @mainpage Prueba_display
 *
 * \section genDesc General Description
 *	initializes the display and show a value.
 * \section hardConn Hardware Connection
 *
 * |   Display   	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * |	GND	 		| 	GND			|
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 29/09/2021 | Document creation		                         |
 *
 * @author Juan Grigolatto
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

