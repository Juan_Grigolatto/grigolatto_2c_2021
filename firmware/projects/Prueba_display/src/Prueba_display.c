/*! @mainpage Prueba_display
 *
 * \section genDesc General Description
 * initializes the display and show a value.
 * \section hardConn Hardware Connection
 *
 * |   Display		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	 		| 	LCD1		|
 * | 	D2	 		| 	LCD2		|
 * | 	D3	 		| 	LCD3		|
 * | 	D4	 		| 	LCD4		|
 * | 	SEL_0	 	| 	GPIO1		|
 * | 	SEL_1	 	| 	GPIO3		|
 * | 	SEL_2	 	| 	GPIO5		|
 * | 	+5V	 		| 	+5V			|
 * |	GND	 		| 	GND			|
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 29/09/2021 | Document creation		                         |
 *
 * @author Juan Grigolatto
 *
 */

/*==================[inclusions]=============================================*/
#include "Prueba_display.h"       /* <= own header */
#include "DisplayITS_E0803.h"
#include "gpio.h"
/*==================[macros and definitions]=================================*/
uint16_t number=256;
uint8_t pins[]={GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

int main(void){

	ITSE0803Init(pins);
	ITSE0803DisplayValue(number);
	ITSE0803Deinit(pins);

	return 0;
}

/*==================[end of file]============================================*/

