/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Prueba_LDR_pro_final.h"       /* <= own header */
#include "uart.h"
#include "delay.h"
#include "LDR.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/
serial_config configuration;
//analog_input_config ad_config={CH1,AINPUTS_SINGLE_READ, NULL};
uint32_t value;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

void SisInit(void)
{

	configuration.port=SERIAL_PORT_PC;
	configuration.baud_rate=115200;
	configuration.pSerial=0;
	SystemClockInit();
	UartInit(&configuration);
	LDRInit(CH1, SINGLE_MODE);
}

int main(void){
	SisInit();



    while(1){

    	value=LDRRead();
    	UartSendString(SERIAL_PORT_PC, "Luminosidad: ");
    	UartSendString(SERIAL_PORT_PC, UartItoa(value, 10));
    	UartSendString(SERIAL_PORT_PC, "\r\n");
    	DelayMs(500);
	}
    
	return 0;
}

/*==================[end of file]============================================*/

