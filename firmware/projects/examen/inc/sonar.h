/*! @mainpage Sonar
 *
 * \section genDesc General Description
 *
 * Esta aplicación se encarga de controlar un sonar, midiendo la distancia en un recorrido de 0 a 75 grados. Además informa la mínima distancia y el ángulo en donde se detecta el objeto mas cercano.
 * tambien controla la dirección de giro del motor del sonar.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| GPIO_T_FIL2   |
 * | 	PIN2	 	| GPIO_T_FIL3   |
 * | 	PIN3	 	| VDDA          |
 * | 	PIN4	 	| GNDA          |
 * | 	PIn5	 	| CH1           |
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021 | Document creation	and proyect creation         |
 * | 			| 	                     						 |
 *
 * @author Juan Grigolatto
 *
 */

#ifndef _SONAR_H
#define _SONAR_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/
/** @fn  void SisInit();
 * @brief Inicializa configuraciones del sistema.
 * @return None
 */
void SisInit();
/** @fn void Measure();
 * @brief Se encarga de medir el valor de distancia y angulo, mostrarlo.
 * @return None
 */
void Measure();
/** @fn void ControlEngine();
 * @brief Se encarga de encender el led RGB cuando el motor gira en un sentido, y cambiarlo cuando gira en otro sentido.
 * @return None
 */
void ControlEngine();

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

