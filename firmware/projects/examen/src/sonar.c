/*! @mainpage Sonar
 *
 * \section Esta aplicación se encarga de controlar un sonar, midiendo la distancia en un recorrido de 0 a 75 grados. Además informa la mínima distancia y el ángulo en donde se detecta el objeto mas cercano.
 * Tambien controla la dirección de giro del motor del sonar.
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| GPIO_T_FIL2   |
 * | 	PIN2	 	| GPIO_T_FIL3   |
 * | 	PIN3	 	| VDDA          |
 * | 	PIN4	 	| GNDA          |
 * | 	PIn5	 	| CH1           |
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/11/2021  | Document creation and proyect creation		                         |
 * |			|							                     |
 *
 * @author Juan Grigolatto
 *
 */

/*==================[inclusions]=============================================*/
#include "sonar.h"       /* <= own header */
#include "uart.h"
#include "hc_sr4.h"
#include "timer.h"
#include "systemclock.h"
#include "led.h"
/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
uint16_t angle=0;
bool half=FALSE;
uint16_t vector_distance[13];
uint16_t actual_distance;
/*==================[internal functions declaration]=========================*/
void SisInit();
void Measure();
void ControlEngine();


/*==================[external data definition]===============================*/
serial_config usb_config;
timer_config my_timer = {TIMER_A,200,&Measure};
/*==================[external functions definition]==========================*/
void SisInit(){
	SystemClockInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
	GoniometroInit(CH1, SINGLE_MODE);
	usb_config.port=SERIAL_PORT_PC;
	usb_config.baud_rate=115200;
	usb_config.pSerial=NULL;
	UartInit(&usb_config);
	LedsInit();


}
void ControlEngine(){
	if(angle=<75){
		LedOn(LED_RGB_R);

	}
	if(angle==75){
		half=TRUE;
	}
	if(half==TRUE){
		LedOn(LED_RGB_G);
	}
}
void Measure(){
	uint8_t i=0;
	uint16_t min_distance=1000000;
	angle=GoniometroRead();
	actual_distance=HcSr04ReadDistanceCentimeters();
	uint8_t pos_min=0;
	uint16 min_in_angle;

	ControlEngine();
	if(angle==0 && half==TRUE){
		half==FALSE;
		for(i=0; i<13; i++){
			if(vector_distance[i]<min_distance){
				max_distance=vector_distance[i];
				pos_min=i;
			}

		}
		if(pos_min==0 || pos_min==7){
			min_in_angle==0;
		}
		if(pos_min==1 || pos_min==8){
			min_in_angle==0;
		}
		if(pos_min==2 || pos_min==9){
			min_in_angle==0;
		}
		if(pos_min==3 || pos_min==10){
			min_in_angle==0;
		}
		if(pos_min==4 || pos_min==11){
			min_in_angle==0;
		}
		if(pos_min==5 || pos_min==12){
			min_in_angle==0;
		}
		if(pos_min==6 || pos_min==13){
			min_in_angle==0;
		}
		UartSendString(SERIAL_PORT_PC, "El objeto mas cercano a");
		UartSendString(SERIAL_PORT_PC, UartItoa(max_distance, 10));
		UartSendString(SERIAL_PORT_PC, "cm en °");
		UartSendString(SERIAL_PORT_PC, UartItoa(max_distance, 10));
	}
	if(angle==0 && half==false){
		vector_distance[0]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==15 && half==false){
		vector_distance[1]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==30 && half==false){
		vector_distance[2]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==45 && half==false){
		vector_distance[3]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==60 && half==false){
		vector_distance[4]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==75 && half==false){
		vector_distance[5]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==90 && half==false){
		vector_distance[6]=HcSr04ReadDistanceCentimeters();

	}

	if(angle==0 && half==true){
		vector_distance[7]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==15 && half==true){
		vector_distance[8]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==30 && half==true){
		vector_distance[9]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==45 && half==true){
		vector_distance[10]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==60 && half==true){
		vector_distance[11]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==75 && half==true){
		vector_distance[12]=HcSr04ReadDistanceCentimeters();

	}
	if(angle==90 && half==true){
		vector_distance[13]=HcSr04ReadDistanceCentimeters();

	}

	UartSendString(SERIAL_PORT_PC, UartItoa(angle, 10));
	UartSendString(SERIAL_PORT_PC, " ° objeto a");
	UartSendString(SERIAL_PORT_PC, UartItoa(actual_distance, 10));
	UartSendString(SERIAL_PORT_PC, "\r\n");

}

int main(void){
	SisInit();

	while(1){

	}

	return 0;
}

/*==================[end of file]============================================*/

