/*! @mainpage Proyecto_final
 *
 * \section genDesc General Description
 *
 * This application control humidity, luminosity and time operation of a camera.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   	LDR			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	CH1			|
 * | 	PIN2	 	| 	VDDA		|
 * | 	PIN3	 	| 	GNDA		|
 *
 * |    LED	white	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1  		| 	T_FIL2 (PWM)|
 * | 	PIN2	 	| 	GND			|
 *
 * |    LED	blue	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1  		| 	T_FIL1 (PWM)|
 * | 	PIN2	 	| 	GND			|
 *
 * |   	DHT11		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC	 		| 	+5V			|
 * | 	DATA	 	| 	GPIO1		|
 * | 	GND	 		| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Juan M. Grigolatto
 *
 */

#ifndef _PROYECTO_FINAL_H
#define _PROYECTO_FINAL_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include "bool.h"
#include <stdint.h>

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/
/** @fn void SisInit()
 * @brief Initializes the elements necessary for the operation of the system
 * @param[in] No parameter
 * @return NULL
 */
void SisInit();
/** @fn void Control()
 * @brief Control when to measure
 * @param[in] No parameter
 * @return NULL
 */
void Control();
/** @fn void MeasureLight()
 * @brief Measure the light in the camera
 * @param[in] No parameter
 * @return NULL
 */
void MeasureLight();
/** @fn void MeasureLight()
 * @brief Measure humidity in the camera
 * @param[in] No parameter
 * @return NULL
 */
void MeasureHumidity();
/** @fn void Menu(void)
 * @brief Show and control the menu of the camera
 * @param[in] No parameter
 * @return NULL
 */
void Menu(void);
/** @fn bool TimeControl()
 * @brief Check if the time is up
 * @param[in] No parameter
 * @return TRUE if time is up
 */
bool TimeControl();
/** @fn uint16_t PorcentToLux(uint16_t porcent_value)
 * @brief Convert percentage to lux
 * @param[in] porcent_value value to convert
 * @return converted value
 */
uint16_t PorcentToLux(uint16_t porcent_value);
/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

