/*! @mainpage Proyecto_final
 *
 * \section genDesc General Description
 *
 * This application control humidity, luminosity and time operation of a camera.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   	LDR			|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	VDDA	    |
 * | 	PIN2	 	| 	CH1		    |
 * | 	PIN3	 	| 	GNDA		|
 *
 * |    LED	white	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1  		| 	T_FIL2 (PWM)|
 * | 	PIN2	 	| 	GND			|
 *
 * |    LED	blue	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1  		| 	T_FIL1 (PWM)|
 * | 	PIN2	 	| 	GND			|
 *
 * |   	DHT11		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC	 		| 	+5V			|
 * | 	DATA	 	| 	GPIO1		|
 * | 	GND	 		| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/11/2021 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Juan M. Grigolatto
 *
 */

/*==================[inclusions]=============================================*/
#include "proyecto_final.h"       /* <= own header */
#include "uart.h"
#include "pwm_sct.h"
#include "timer.h"
#include "sapi_rtc.h"
#include "systemclock.h"
#include <stdint.h>
#include "LDR.h"
#include "sapi_dht11.h"
#include "led.h"
/*==================[macros and definitions]=================================*/
#define MAX_LUX_MENOS_CUARENTA 500  /* <= Lux max value -50 */
#define MIN_LUX 50					/* <= Lux min value */
/*==================[internal data definition]===============================*/
bool time_end=false;  				/* <= Check if time end  */
bool flag= false;     				/* <= Check if the parameters should be regulated and measure  */
bool stop_time=false;  				/* <= Check if time stop */
bool flag_control_time=false; 		/* <= Check if time stop */
bool finish_time_control=false;		/* <= Check if time control finish */

uint8_t timer_measure=0;			/* <= counter for measure 1 second*/

uint16_t light_value;				/* <= Saves light value in camera */
uint16_t light_value_prom=0;		/* <= Saves light value average in camera*/
float humidity_value;				/* <= Saves humidity value in camera*/
float temperature_value;			/* <= Saves temperature value (unused)*/
uint8_t pwm_cycle_value_light=50;	/* <= Saves PWM cycle value of light in camera*/
uint8_t pwm_cycle_value_hum=50;		/* <= Saves PWM cycle value of humidity in camera*/

bool set_light_flag=FALSE;			/* <= Check if the light value should be entered*/
bool set_humidity_flag=FALSE;		/* <= Check if the humidity value should be entered*/
bool set_time_flag=FALSE;			/* <= Check if the actual time value should be entered*/
bool set_operation_time_flag=FALSE; /* <= Check if the operation time value should be entered*/
bool camera_on=FALSE;				/* <= Check if the camera is on*/

bool light_value_in=FALSE;			/* <= Check if the light value was entered*/
bool humidity_value_in=FALSE;		/* <= Check if the humidity value was entered*/
bool operation_time_in=FALSE;		/* <= Check if the operation time value was entered*/

uint16_t set_light = 0;				/* <= Saves the light value entered by the user */
uint16_t set_humidity = 0;			/* <= Saves the humidity value entered by the user */
/*==================[internal functions declaration]=========================*/
void SisInit();						/* <= Initializes the elements necessary for the operation of the system */
void Control();						/* <= Control when to measure*/
void MeasureLight();				/* <= Measure the light in the camera*/
void MeasureHumidity();				/* <= Measure humidity in the camera*/
void Menu(void);					/* <= system menu*/
bool TimeControl();					/* <=Check if the time is up*/
uint16_t PorcentToLux(uint16_t porcent_value); /* <=Convert percentage to lux*/
/*==================[external data definition]===============================*/
timer_config timer_configuration={TIMER_A, 10, &Control};
serial_config uart_configuration={SERIAL_PORT_PC,115200,&Menu};

pwm_out_t pwm_configuration[]={CTOUT0,CTOUT1};

rtc_t hour_config;
rtc_t operation_time_config;
typedef enum  {hour_p, minutes_p, day_p, month_p, year_p} time_t;


time_t current_time=hour_p;
time_t operation_time=hour_p;

/*==================[external functions definition]==========================*/
void SisInit(){
	TimerInit(&timer_configuration);
	TimerStart(TIMER_A);
	UartInit(&uart_configuration);
	LDRInit(CH1,SINGLE_MODE);
	dht11Init(GPIO_1);
	PWMInit(pwm_configuration, 2, 1000);
	PWMOn();
	LedsInit();

}
void Menu(void){

	uint8_t data;
	UartReadByte(SERIAL_PORT_PC, &data);

	if(set_humidity_flag){
		set_humidity = data;
		if((set_humidity<=100) && (set_humidity>=0)){
			UartSendString(SERIAL_PORT_PC, "Humedad relativa:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(data, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			data = 0;
			set_humidity_flag = FALSE;
			humidity_value_in = TRUE;
		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato invalido...ingrese un valor entre 0 y 100\r\n");
			data = 0;
		}
	}
	if(set_light_flag){
		set_light = data;
		if((set_light<=100) && (set_light>=0)){
			set_light=PorcentToLux(set_light);
			UartSendString(SERIAL_PORT_PC, "Luminosidad:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(data, 10));
			UartSendString(SERIAL_PORT_PC, " \r\n");
			data = 0;
			set_light_flag = FALSE;
			light_value_in = TRUE;

		}
		else{
			UartSendString(SERIAL_PORT_PC, "Dato invalido...ingrese un valor entre 0 y 100 %\r\n");
			data = 0;
		}
	}
	if(set_time_flag){
		switch(current_time){
		case hour_p:
			hour_config.hour = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese la minutos actuales:  \r\n");
			current_time = minutes_p;
			break;
		case minutes_p:
			hour_config.min = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese el dia actuales:  \r\n");
			current_time = day_p;
			break;
		case day_p:
			hour_config.mday = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese el mes actual:  \r\n");
			current_time = month_p;
			break;
		case month_p:
			hour_config.month = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese el anio actual:  \r\n");
			current_time = year_p;
			break;
		case year_p:
			hour_config.year = data;
			rtcConfig(&hour_config);
			flag_control_time=TRUE;
			UartSendString(SERIAL_PORT_PC, "Son las:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.min, 10));
			UartSendString(SERIAL_PORT_PC, "  del:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.mday, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.month, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hour_config.year, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			current_time = hour_p;
			data = 0;
			set_time_flag = FALSE;
			LedOff(LED_RGB_B);
			LedOn(LED_RGB_G);
			camera_on=true;
			break;
		}
	}

	if(set_operation_time_flag){
		switch(operation_time){
		case hour_p:
			operation_time_config.hour = data;
			UartSendString(SERIAL_PORT_PC, "Ingrese los minutos:  \r\n");
			operation_time = minutes_p;
			break;
		case minutes_p:
			operation_time_config.min = data;
			operation_time_config.mday= hour_config.mday;
			operation_time_config.month= hour_config.month;
			operation_time_config.year= hour_config.year;
			operation_time = hour_p;
			data = 0;
			set_operation_time_flag= FALSE;
			operation_time_in = TRUE;
			UartSendString(SERIAL_PORT_PC, "Tiempo de operacion:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(operation_time_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(operation_time_config.min, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			stop_time=FALSE;
			break;
		}
	}
	switch(data){
	case 'H': //initializes humidity value
		UartSendString(SERIAL_PORT_PC, "Ingrese la humedad relativa:\r\n");
		set_humidity_flag= TRUE;
		break;
	case 'L': //initializes luminosity value
		UartSendString(SERIAL_PORT_PC, "Ingrese el valor de luminosidad en porcentaje (0 a 100%):\r\n");
		set_light_flag= TRUE;
		break;
	case 'I': //Start camera and initialize current time
		if(light_value_in==TRUE && humidity_value_in==TRUE && operation_time_in==TRUE){
			UartSendString(SERIAL_PORT_PC, "Camara encendida\r\n");
			UartSendString(SERIAL_PORT_PC, "Ingrese la hora actual:\r\n");
			set_time_flag= TRUE;
			LedOn(LED_RGB_B);
		}
		else{
			if(!light_value_in){
				UartSendString(SERIAL_PORT_PC, "Error: ingrese la luminosidad\r\n");
			}
			if(!humidity_value_in){
				UartSendString(SERIAL_PORT_PC, "Error: ingrese la humedad relativa\r\n");
			}
			if(!operation_time_in){
				UartSendString(SERIAL_PORT_PC, "Error: ingrese el tiempo de operacion\r\n");
			}
			UartSendString(SERIAL_PORT_PC, "Luego, vuelva a intentarlo\r\n");

		}
		break;
	case 'F': //Camera program end
		if(camera_on){
			camera_on=false;
			UartSendString(SERIAL_PORT_PC, "Camara apagada\r\n");
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_RGB_G);
			LedOff(LED_RGB_B);
			PWMOff();

		}
		else{
			UartSendString(SERIAL_PORT_PC, "Primero debe encender la camara\r\n");
		}
		break;
	case 'D': //Initializes operation value
		UartSendString(SERIAL_PORT_PC, "Ingrese las horas:\r\n");
		set_operation_time_flag= TRUE;
		break;
	case 'h': //Recovers humidity value
		UartSendString(SERIAL_PORT_PC, "Humedad relativa:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(set_humidity, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		break;
	case 'l': //Recovers light value
		UartSendString(SERIAL_PORT_PC, "Luminosidad:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(set_light, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		break;
	case 'd': //Recovers operation time
		UartSendString(SERIAL_PORT_PC, "Tiempo de operacion:  ");
		UartSendString(SERIAL_PORT_PC, UartItoa(operation_time_config.hour, 10));
		UartSendString(SERIAL_PORT_PC, ":");
		UartSendString(SERIAL_PORT_PC, UartItoa(operation_time_config.min, 10));
		UartSendString(SERIAL_PORT_PC, "\r\n");
		break;
	case 'M': //Show commands
		UartSendString(SERIAL_PORT_PC, "/---------------MENU------------/ \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'I' para encender la camara y configurar la hora \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'F' para apagar la camara\r\n");

		UartSendString(SERIAL_PORT_PC, "Ingrese 'H' para configurar la humedad relativa: \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'L' para configurar la luminosidad: \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'D' para configurar la hora de finalizacion: \r\n");

		UartSendString(SERIAL_PORT_PC, "Ingrese 'h' para recuperar la humedad relativa: \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'l' para recuperar la luminosidad: \r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'd' para recuperar la hora de finalizacion: \r\n");
		UartSendString(SERIAL_PORT_PC, "/-------------------------------/ \r\n");
		break;
	}
}


void Control(){
	flag=true;


}

void MeasureLight(){
	light_value=LDRRead();
}

uint16_t PorcentToLux(uint16_t porcent_value){
	uint16_t new_lux_value=0;
	new_lux_value=((porcent_value*MAX_LUX_MENOS_CUARENTA)/100)+MIN_LUX;
	return new_lux_value;
}

bool TimeControl(){

	if(flag_control_time){
		rtcRead(&hour_config);
		uint8_t minutes;
		uint8_t hours;
		hours=operation_time_config.hour-hour_config.hour;
		minutes=operation_time_config.min-hour_config.min;
		if(hours==0 && minutes==0){
			stop_time=TRUE;
		}
		else{
			stop_time=false;
		}
	}

	return stop_time;
}

void MeasureHumidity(){
	dht11Read(&humidity_value, &temperature_value);
	humidity_value=(uint8_t)humidity_value;
}


int main(void){
	SystemClockInit();
	SisInit();

	UartSendString(SERIAL_PORT_PC, "Ingrese 'M' para acceder al menu de operacion\r\n");
	while(1){
		time_end=TimeControl();
		if((camera_on)){
			if(!finish_time_control){
				if(!time_end){
					if((flag==true)){
						timer_measure++;
						MeasureLight();
						MeasureHumidity();
						light_value_prom+=light_value;

						if(light_value>set_light){
							pwm_cycle_value_light=pwm_cycle_value_light-1;
							PWMSetDutyCycle(CTOUT0, pwm_cycle_value_light);
						}
						if(light_value<set_light){
							pwm_cycle_value_light=pwm_cycle_value_light+1;
							PWMSetDutyCycle(CTOUT0, pwm_cycle_value_light);
						}

						if(humidity_value<set_humidity){
							LedOn(LED_1);
							LedOff(LED_2);
							pwm_cycle_value_hum=pwm_cycle_value_hum+1;
							PWMSetDutyCycle(CTOUT1, pwm_cycle_value_hum);
						}
						if(humidity_value>set_humidity){
							LedOn(LED_2);
							LedOff(LED_1);
							pwm_cycle_value_hum=pwm_cycle_value_hum-1;
							PWMSetDutyCycle(CTOUT1, pwm_cycle_value_hum);
						}
						if(humidity_value==set_humidity){
							LedOff(LED_1);
							LedOff(LED_2);
						}
						flag=false;
					}
					//timer_measure == 50 es el valor estimado a prueba y error para que los valores se muestren cada un segundo
					if(timer_measure==50){
						light_value_prom=(light_value_prom/50);

						UartSendString(SERIAL_PORT_PC, "Luminosidad en camara: ");
						UartSendString(SERIAL_PORT_PC, UartItoa(light_value_prom, 10));
						UartSendString(SERIAL_PORT_PC, " lux \r\n");

						UartSendString(SERIAL_PORT_PC, "Humedad en camara: ");
						UartSendString(SERIAL_PORT_PC, UartItoa(humidity_value, 10));
						UartSendString(SERIAL_PORT_PC, " % \r\n");

						light_value_prom=0;

						timer_measure=0;
					}
				}
				else{
					UartSendString(SERIAL_PORT_PC, "Finalizo el tiempo de operacion \r\n");
					finish_time_control=TRUE;
				}
			}
		}
	}
	return 0;
}

/*==================[end of file]============================================*/

