/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "Prueba_DHT22_pro_final.h"       /* <= own header */
#include "uart.h"
#include "delay.h"
#include "DHT22.h"
#include "sapi_dht11.h"
#include "systemclock.h"
/*==================[macros and definitions]=================================*/
serial_config configuration;
float humidity;
float temp;
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
void SisInit(void)
{

	configuration.port=SERIAL_PORT_PC;
	configuration.baud_rate=115200;
	configuration.pSerial=0;
	SystemClockInit();
	UartInit(&configuration);
	//DHTInit(GPIO_1);
	dht11Init(GPIO_1);

}

int main(void){
	SisInit();
	while(1){
		//value=DHTRead();
		dht11Read(&humidity, &temp);
		UartSendString(SERIAL_PORT_PC, "RH: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(humidity, 10));
		UartSendString(SERIAL_PORT_PC, " \r\n");
		DelayMs(500);
	}

	return 0;
}

/*==================[end of file]============================================*/

