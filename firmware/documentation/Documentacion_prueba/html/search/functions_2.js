var searchData=
[
  ['hcsr04deinit_144',['HcSr04Deinit',['../group___ultrasonido.html#ga8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___ultrasonido.html#ga8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04init_145',['HcSr04Init',['../group___ultrasonido.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___ultrasonido.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04readdistancecentimeters_146',['HcSr04ReadDistanceCentimeters',['../group___ultrasonido.html#ga30b2210bfe5f719a536c61c1186b9f52',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c'],['../group___ultrasonido.html#ga30b2210bfe5f719a536c61c1186b9f52',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c']]],
  ['hcsr04readdistanceinches_147',['HcSr04ReadDistanceInches',['../group___ultrasonido.html#gab133fb60f6039c8a17e14eb821296ccd',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c'],['../group___ultrasonido.html#gab133fb60f6039c8a17e14eb821296ccd',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c']]]
];
