var searchData=
[
  ['hcsr04deinit_74',['HcSr04Deinit',['../group___ultrasonido.html#ga8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___ultrasonido.html#ga8a7c7627f3588d259b82195478b48767',1,'HcSr04Deinit(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04init_75',['HcSr04Init',['../group___ultrasonido.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c'],['../group___ultrasonido.html#ga9d26cc017fe45c607d08231ebffb46c4',1,'HcSr04Init(gpio_t echo, gpio_t trigger):&#160;hc_sr4.c']]],
  ['hcsr04readdistancecentimeters_76',['HcSr04ReadDistanceCentimeters',['../group___ultrasonido.html#ga30b2210bfe5f719a536c61c1186b9f52',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c'],['../group___ultrasonido.html#ga30b2210bfe5f719a536c61c1186b9f52',1,'HcSr04ReadDistanceCentimeters(void):&#160;hc_sr4.c']]],
  ['hcsr04readdistanceinches_77',['HcSr04ReadDistanceInches',['../group___ultrasonido.html#gab133fb60f6039c8a17e14eb821296ccd',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c'],['../group___ultrasonido.html#gab133fb60f6039c8a17e14eb821296ccd',1,'HcSr04ReadDistanceInches(void):&#160;hc_sr4.c']]],
  ['hwpin_78',['hwPin',['../structdigital_i_o.html#a93d2e4a48daa464205632175fdb7288c',1,'digitalIO']]],
  ['hwport_79',['hwPort',['../structdigital_i_o.html#a79691c4619ba92dbf4859aaa6e006531',1,'digitalIO']]]
];
