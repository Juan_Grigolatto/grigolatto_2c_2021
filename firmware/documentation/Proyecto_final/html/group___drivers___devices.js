var group___drivers___devices =
[
    [ "Bool", "group___bool.html", "group___bool" ],
    [ "Delay", "group___delay.html", "group___delay" ],
    [ "Led", "group___l_e_d.html", "group___l_e_d" ],
    [ "Display", "group___display.html", "group___display" ],
    [ "goniometro", "group___g_o_n_i_o_m_e_t_r_o.html", "group___g_o_n_i_o_m_e_t_r_o" ],
    [ "Ultrasonido", "group___ultrasonido.html", "group___ultrasonido" ],
    [ "LDR", "group___l_d_r.html", "group___l_d_r" ],
    [ "sapi_dht11", "group___d_h_t11.html", "group___d_h_t11" ],
    [ "Switch", "group___switch.html", "group___switch" ],
    [ "Infrarrojo", "group___infrarrojo.html", "group___infrarrojo" ],
    [ "Analog IO", "group___analog___i_o.html", "group___analog___i_o" ]
];