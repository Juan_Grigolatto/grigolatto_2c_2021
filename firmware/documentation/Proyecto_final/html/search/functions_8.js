var searchData=
[
  ['tcrt5000deinit_219',['Tcrt5000Deinit',['../group___infrarrojo.html#gaf64bb75b4a2cea914208f99c462abeeb',1,'Tcrt5000Deinit(gpio_t dout):&#160;Tcrt5000.c'],['../group___infrarrojo.html#gaf64bb75b4a2cea914208f99c462abeeb',1,'Tcrt5000Deinit(gpio_t dout):&#160;Tcrt5000.c']]],
  ['tcrt5000init_220',['Tcrt5000Init',['../group___infrarrojo.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;Tcrt5000.c'],['../group___infrarrojo.html#gab4bfab14f44be848c953f96363908f96',1,'Tcrt5000Init(gpio_t dout):&#160;Tcrt5000.c']]],
  ['tcrt5000state_221',['Tcrt5000State',['../group___infrarrojo.html#ga51671fa99fcdfd5bfb58feb05fca49b9',1,'Tcrt5000State(void):&#160;Tcrt5000.c'],['../group___infrarrojo.html#ga51671fa99fcdfd5bfb58feb05fca49b9',1,'Tcrt5000State(void):&#160;Tcrt5000.c']]],
  ['timerinit_222',['TimerInit',['../group___baremetal.html#ga148b01475111265d1798f5c204a93df0',1,'timer.c']]],
  ['timerreset_223',['TimerReset',['../group___baremetal.html#ga479d496a6ad7a733fb8da2f36800b76b',1,'timer.c']]],
  ['timerstart_224',['TimerStart',['../group___baremetal.html#ga31487bffd934ce838a72f095f9231b24',1,'timer.c']]],
  ['timerstop_225',['TimerStop',['../group___baremetal.html#gab652b899be3054eae4649a9063ec904b',1,'timer.c']]],
  ['tolux_226',['ToLux',['../group___l_d_r.html#gad87d680d96e232bcb700403274357ec3',1,'ToLux(uint16_t value_to_lux):&#160;LDR.c'],['../group___l_d_r.html#gad87d680d96e232bcb700403274357ec3',1,'ToLux(uint16_t value_to_lux):&#160;LDR.c']]]
];
