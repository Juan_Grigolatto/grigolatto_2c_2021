var searchData=
[
  ['itse0803deinit_196',['ITSE0803Deinit',['../group___display.html#ga261a7982d78c66d57715fc6d12451cd4',1,'ITSE0803Deinit(gpio_t *pins):&#160;DisplayITS_E0803.c'],['../group___display.html#ga261a7982d78c66d57715fc6d12451cd4',1,'ITSE0803Deinit(gpio_t *pins):&#160;DisplayITS_E0803.c']]],
  ['itse0803displayvalue_197',['ITSE0803DisplayValue',['../group___display.html#ga7a77359e3bccda99b1d684c97f331c16',1,'ITSE0803DisplayValue(uint16_t valor):&#160;DisplayITS_E0803.c'],['../group___display.html#ga7a77359e3bccda99b1d684c97f331c16',1,'ITSE0803DisplayValue(uint16_t valor):&#160;DisplayITS_E0803.c']]],
  ['itse0803init_198',['ITSE0803Init',['../group___display.html#ga5f8938281bcbf8fbcea503c6967dde22',1,'ITSE0803Init(gpio_t *pins):&#160;DisplayITS_E0803.c'],['../group___display.html#ga5f8938281bcbf8fbcea503c6967dde22',1,'ITSE0803Init(gpio_t *pins):&#160;DisplayITS_E0803.c']]],
  ['itse0803readvalue_199',['ITSE0803ReadValue',['../group___display.html#ga3b58e33b98bd04e1a34d451b305f885d',1,'ITSE0803ReadValue(void):&#160;DisplayITS_E0803.c'],['../group___display.html#ga3b58e33b98bd04e1a34d451b305f885d',1,'ITSE0803ReadValue(void):&#160;DisplayITS_E0803.c']]]
];
