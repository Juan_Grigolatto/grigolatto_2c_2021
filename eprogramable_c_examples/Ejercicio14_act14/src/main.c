/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


/*
Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20 caracteres y edad.
a)Defina una variable con esa estructura y cargue los campos con sus propios datos.
b)Defina un puntero a esa estructura y cargue los campos con los datos de su compañero (usando acceso por punteros).
*/

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include "string.h"
/*==================[macros and definitions]=================================*/
//Declaro una estructura llamada Alumno, que contiene el nombre, apellido y edad.
 typedef struct {
	char nombre [12]; //usar uint8_t nombre [12]
	char apellido [20]; //usar uint8_t apellido [20]
	uint8_t edad;
} Alumno;


/*==================[internal functions declaration]=========================*/

int main(void)
{
	//Declaro una un estudiante y un puntero a estudiante.
	Alumno student, *ptr_student;
	//Cargo el nombre del estudiante mediante el operador de acceso.
	strcpy(student.nombre,"Juan");
	//Cargo el apellido del estudiante mediante el operador de acceso.
	strcpy(student.apellido, "Grigolatto");
	//Cargo la edad del estudiante mediante el operador de acceso.
	student.edad=21;
	//Muestro los datos del estudiante.
	printf("Nombre: %s\n\r", student.nombre);
	printf("Apellido: %s\n\r",student.apellido);
	printf("Edad: %d\n\r",student.edad);
	//Cargo en el puntero a la estructura los datos de un nuevo estudiante
	strcpy(ptr_student->nombre,"Martin");
	strcpy(ptr_student->apellido,"suarez");
	ptr_student->edad=21;
	//Muestro el nuevo estudiante
	printf("Nombre: %s\n\r", ptr_student->nombre);
	printf("Apellido: %s\n\r",ptr_student->apellido);
	printf("Edad: %d\n\r",ptr_student->edad);
	return 0;
}

/*==================[end of file]============================================*/

