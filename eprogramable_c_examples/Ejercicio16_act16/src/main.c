/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
a)Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare cuatro variables sin signo de 8 bits y,
utilizando máscaras, rotaciones y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
b)Realice el mismo ejercicio, utilizando la definición de una “union”.
*/


/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/
//Declaro la variable y le cargo 0x01020304
uint32_t variable = 0x01020304;
#define BIT_8 8;
#define BIT_16 16;
#define BIT_24 24;
uint8_t var_1;
uint8_t var_2;
uint8_t var_3;
uint8_t var_4;
uint8_t aux;
//Defino la unión.
union bytes{
	//Declaro struct que contiene los 4 bits.
	struct {
		uint8_t byte_1;
		uint8_t byte_2;
		uint8_t byte_3;
		uint8_t byte_4;
	} cada_byte;
	uint32_t todos_los_bytes;
};
/*==================[internal functions declaration]=========================*/

int main(void)
{
	//Trunco la variable de 32 bits, obteniendo los primeros 8 bits.
	var_1=(uint8_t) variable;
	//Roto la variable 8 bits hacia la izquierda.
	aux=variable>>BIT_8;
	//Trunco la variable de 32 bits, obteniendo los 8 bits siguientes.
	var_2= (uint8_t) aux;
	//Roto la variable al bit 16.
	aux=variable>>BIT_16;
	//Trunco la variable de 32 bits, obteniendo los 8 bits siguientes.
	var_3= (uint8_t) aux;
	//Roto la variable al bit 24.
	aux=variable>>BIT_24;
	//Trunco la variable de 32 bits, obteniendo los 8 bits siguientes.
	var_4= (uint8_t) aux;

	//Muestro por pantalla cada byte obtenido.
	printf("Byte 1: %u\n\r",var_1);
	printf("Byte 2: %u\n\r",var_2);
	printf("Byte 3: %u\n\r",var_3);
	printf("Byte 4: %u\n\r",var_4);

	//Declaro la unión.
	union bytes uni_variable;
	//Guardo la variable.
	uni_variable.todos_los_bytes= variable;
	//Muestro por pantalla haciendo uso de la unión cada uno de los bits.
	printf("Byte union 1: %u\n\r", uni_variable.cada_byte.byte_1);
	printf("Byte union 2: %u\n\r", uni_variable.cada_byte.byte_2);
	printf("Byte union 3: %u\n\r", uni_variable.cada_byte.byte_3);
	printf("Byte union 4: %u\n\r", uni_variable.cada_byte.byte_4);
	return 0;
}

/*==================[end of file]============================================*/

