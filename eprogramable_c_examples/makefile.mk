########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ejercicio9_act9
#NOMBRE_EJECUTABLE = hola_mundo.exe

#PROYECTO_ACTIVO = Ejercicio7_act7
#NOMBRE_EJECUTABLE = Ejercicio7_act7.exe

# Ruta del Proyecto
####Ejemplo 9: Ejercicio9_act9
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ejercicio9_act9
#NOMBRE_EJECUTABLE = Ejercicio9_act9.exe

# Ruta del Proyecto
####Ejemplo 12: Ejercicio12_act12
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ejecicio12_act12
#NOMBRE_EJECUTABLE = Ejecicio12_act12.exe

##PROYECTO_ACTIVO = Ejercicio14_act14
##NOMBRE_EJECUTABLE = Ejercicio14_act14.exe

#PROYECTO_ACTIVO = Ejercicio16_act16
#NOMBRE_EJECUTABLE = Ejercicio16_act16.exe

#PROYECTO_ACTIVO = Ejercicio17_act17
#NOMBRE_EJECUTABLE = Ejercicio17_act17.exe

####Ejemplo 1: Ejercicio1_act1
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ejecicio1_act1
#NOMBRE_EJECUTABLE = Ejecicio1_act1.exe

####Ejemplo 2: Ejercicio2_act2
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ejecicio2_act2
#NOMBRE_EJECUTABLE = Ejecicio2_act2.exe

####Ejemplo 3: Ejercicio3_act3
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ejecicio3_act3
#NOMBRE_EJECUTABLE = Ejecicio2_act3.exe

####Ejemplo 7: Ejercicio7_act7
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = Ejecicio7_act7
#NOMBRE_EJECUTABLE = Ejecicio7_act7.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

##PROYECTO_ACTIVO = EjercicioA_integrador
##NOMBRE_EJECUTABLE = EjercicioA_integrador.exe


PROYECTO_ACTIVO = EjercicioC_integrador
NOMBRE_EJECUTABLE = EjercicioC_integrador.exe

#PROYECTO_ACTIVO = EjercicioD_integrador
#NOMBRE_EJECUTABLE = EjercicioD_integrador.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe
