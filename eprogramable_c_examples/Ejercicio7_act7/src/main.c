/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


//Sobre una variable de 32 bits sin signo previamente declarada y de valor desconocido,
//asegúrese de colocar el bit 3 a 1 y los bits 13 y 14 a 0 mediante máscaras y el operador <<.




/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
/*==================[macros and definitions]=================================*/
#define BIT_3 3
#define BIT_14 14
#define BIT_13 13
//Declaro una variable que contiene unos.
uint32_t variable = 0xFFFF;
/*==================[internal functions declaration]=========================*/

int main(void)
{
	//Declaro una máscara con un 1 en el bit 13 y otro en el bit 14.
    uint32_t mascara_ceros = (1<<BIT_13) | (1>>BIT_14);
    //Invierto la máscara.
    mascara_ceros = ~mascara_ceros;
    //Declaro una máscara con un 1 en el bit 3.
    uint32_t mascara_uno = 1 << BIT_3;
    //Muestro las máscaras.
    printf("Mascara ceros: %u\r\n", mascara_ceros);
    printf("Mascara uno: %u\r\n", mascara_uno);
    //Guardo en variable el resultado de la operación AND entre la primer máscara y la variable.
    variable= variable & mascara_ceros;
    //Guando en variable el resultado de la operación OR entre la segunda máscara y la variable.
    variable= variable | mascara_uno;
    printf("Variable: %u\r\n", variable);

	return 0;
}

/*==================[end of file]============================================*/

