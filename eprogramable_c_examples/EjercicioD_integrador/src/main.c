/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;


gpioConf_t vec[]={{1,4,1},{1,5,1},{1,6,1},{2,14,1}};


uint8_t num_test;
uint32_t data_test;
uint8_t digit_test;
uint8_t number_bcd[4];
uint8_t j;
void Bcd (uint8_t number, gpioConf_t *vector){

	uint8_t mascara=1;
	uint8_t auxiliar=0;
	auxiliar=number&mascara;
	if(auxiliar){
		printf("Se pone en 1 el puerto %u, Pin %u, Direccion %u\r\n", vector[0].port, vector[0].pin,  vector[0].dir);
	}
	else{
		printf("Se pone en 0 el puerto %u, Pin %u, Direccion %u\r\n", vector[0].port, vector[0].pin, vector[0].dir);
	}
	mascara=mascara << 1;
	auxiliar=number&mascara;
	if(auxiliar){
		printf("Se pone en 1 el puerto %u, Pin %u, Direccion %u\r\n", vector[1].port, vector[1].pin,  vector[1].dir);
	}
	else{
		printf("Se pone en 0 el puerto %u, Pin %u, Direccion %u\r\n", vector[1].port, vector[1].pin,  vector[1].dir);

	}
	mascara=mascara << 1;
	auxiliar=number&mascara;
	if(auxiliar){
		printf("Se pone en 1 el puerto %u, Pin %u, Direccion %u\r\n", vector[2].port, vector[2].pin,  vector[2].dir);
	}
	else{
		printf("Se pone en 0 el puerto %u, Pin %u, Direccion %u\r\n", vector[2].port, vector[2].pin,  vector[2].dir);
	}
	mascara=mascara << 1;
	auxiliar=number&mascara;
	if(auxiliar){
		printf("Se pone en 1 el puerto %u, Pin %u, Direccion %u\r\n", vector[3].port, vector[3].pin,  vector[3].dir);
	}
	else{
		printf("Se pone en 0 el puerto %u, Pin %u, Direccion %u\r\n", vector[3].port, vector[3].pin,  vector[3].dir);
	}
}

void  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t i=0, aux;
	uint32_t aux_num;
	aux_num=data;
	for(i=0;i<digits; i++){
	aux=(aux_num%10);
	aux_num=(aux_num/10);
	bcd_number[i]=aux;

	printf("Nº %d\r\n", bcd_number[i]);
	}

}
/*==================[internal functions declaration]=========================*/

int main(void)
{
	digit_test=4;
	data_test=1234;
	BinaryToBcd(data_test, digit_test, number_bcd);
	for(j=0;j<digit_test;j++){
		Bcd(number_bcd[j], vec);
	}


	return 0;
}

/*==================[end of file]============================================*/

